﻿using System;
using System.Collections;
using UnityEngine;

public class InGameLoad : AState
{
    [SerializeField]
    private GameObject _LoadUICanvas;
    [SerializeField]
    private MusicPlayer _musicPlayer;

    private int k_UILayer;
    public MeshFilter skyMeshFilter;
    public MeshFilter UIGroundFilter;
    protected bool m_IsLoadingCharacter;
    protected Modifier m_CurrentModifier = new Modifier();

    public override void Enter(AState from)
    {
        _LoadUICanvas.SetActive(true);

        k_UILayer = LayerMask.NameToLayer("UI");

        skyMeshFilter.gameObject.SetActive(true);
        UIGroundFilter.gameObject.SetActive(true);

        // Reseting the global blinking value. Can happen if the game unexpectedly exited while still blinking
        Shader.SetGlobalFloat("_BlinkingValue", 0.0f);
        Debug.Log("InGame Load Enter");
        StartCoroutine(Populate());
    }

    private IEnumerator Populate()
    {
        Debug.Log("InGame Load populate start");

        AssetBundlesDatabaseHandler.Load();

        yield return new WaitForSeconds(1f);

        yield return CoroutineHandler.StartStaticCoroutine(PopulateTheme());

        Debug.Log("InGame Load populate");
        yield return new WaitForSeconds(0.05f);
        StartGame();


        yield return null;
        
    }

    public IEnumerator PopulateCharacters()
    {
        if (!m_IsLoadingCharacter)
        {
            m_IsLoadingCharacter = true;
            GameObject newChar = null;
            while (newChar == null)
            {
                Character c = CharacterDatabase.GetCharacter(PlayerData.instance.characters[PlayerData.instance.usedCharacter]);
                yield return new WaitForSeconds(1.0f);
            }
            m_IsLoadingCharacter = false;
        }
    }

    public IEnumerator PopulateTheme()
    {
        ThemeData t = null;

        while (t == null)
        {
            t = ThemeDatabase.GetThemeData(PlayerData.instance.themes[PlayerData.instance.usedTheme]);
            yield return null;
        }
        skyMeshFilter.sharedMesh = t.skyMesh;
        UIGroundFilter.sharedMesh = t.UIGroundMesh;
    }


    public void StartGame()
    {
        Debug.Log("InGame Load");
        _musicPlayer.musicVolume = EndlessRunSingleton.Instance.MusicVolume;
        _musicPlayer.sfxVolume = EndlessRunSingleton.Instance.SFXVolume;
        CoroutineHandler.StartStaticCoroutine(MusicPlayer.instance.RestartAllStems());

        manager.SwitchState("InGamePlay");  
    }

    public override void Exit(AState to)
    {
        _LoadUICanvas.SetActive(false);

        GameState gs = to as GameState;

        skyMeshFilter.gameObject.SetActive(false);
        UIGroundFilter.gameObject.SetActive(false);

    }
    
    public override string GetName()
    {
        return "InGameLoad";
    }

    public override void Tick()
    {
        
    }
}
