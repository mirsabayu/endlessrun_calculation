﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGamePlay : AState
{
    [SerializeField]
    private GameObject _InGamePlayUI;

    public TrackManager trackManager;
    public AudioClip gameTheme;

    [Header("UI")]
    public Text scoreText;
    public Text countdownText;

    
    protected RectTransform m_CountdownRectTransform;
    protected bool m_GameoverSelectionDone = false;
    protected float m_TimeSinceStart;
    public Modifier currentModifier = new Modifier();
    protected bool m_Finished;
    protected bool m_IsStumbled;

    protected float _spawnQuestionCounter;
    protected float _spawnQuestionLimit;

    public override void Enter(AState from)
    {
        _InGamePlayUI.SetActive(true);
        m_CountdownRectTransform = countdownText.GetComponent<RectTransform>();
        QuestionManager.Instance.QuestionAnswerWrongEvent.AddListener(QuestionAnswerWrong);

        m_GameoverSelectionDone = false;

        Debug.Log("In Game Play");

        _spawnQuestionCounter = 0;
        _spawnQuestionLimit = 5f;

        StartGame();
        
    }

    private void QuestionAnswerWrong()
    {
        manager.SwitchState("InGameOver");
    }

    public void StartGame()
    {

        if (!trackManager.isRerun)
        {
            m_TimeSinceStart = 0;
            trackManager.characterController.currentLife = trackManager.characterController.maxLife;
        }

        currentModifier.OnRunStart(this);
        trackManager.Begin();

        m_Finished = false;
        m_IsStumbled = false;

        QuestionManager.Instance.BeginQuestion();
    }

    public override void Exit(AState to)
    {
        QuestionManager.Instance.QuestionAnswerWrongEvent.RemoveListener(QuestionAnswerWrong);
        _InGamePlayUI.SetActive(false);

    }

    public override string GetName()
    {
        return "InGamePlay";
    }

    public override void Tick()
    {
        CharacterInputController chrCtrl = trackManager.characterController;
        UpdateUI();
    }

    protected void UpdateUI()
    {
        scoreText.text = trackManager.score.ToString();

        if (trackManager.timeToStart >= 0)
        {
            countdownText.gameObject.SetActive(true);
            countdownText.text = Mathf.Ceil(trackManager.timeToStart).ToString();
            m_CountdownRectTransform.localScale = Vector3.one * (1.0f - (trackManager.timeToStart - Mathf.Floor(trackManager.timeToStart)));
        }
        else
        {
            m_CountdownRectTransform.localScale = Vector3.zero;
        }
    }

    protected void QuestionsMechanics(float deltaTime)
    {
        if(_spawnQuestionCounter < _spawnQuestionLimit)
        {
            _spawnQuestionCounter += deltaTime;
        }
        else if(QuestionManager.Instance.IsQuestionAvailable)
        {
            _spawnQuestionCounter = 0;
            trackManager.SpawnQuestionTrigger(trackManager.segments[1]);
        }
        
    }
}
