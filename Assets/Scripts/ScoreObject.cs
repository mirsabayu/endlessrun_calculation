﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ScoreObject : MonoBehaviour
{
    [SerializeField]
    private Text Name;
    [SerializeField]
    private Text Score;


    public void SetScore(string name, string score)
    {
        Name.text = name;
        Score.text = score;
    }

}
