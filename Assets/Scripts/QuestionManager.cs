﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[Serializable]
public class Question
{
    
    public int Id;
    public string Questions;
    public string Answer;
    public string[] wrongAnswer;
    public int Level;

    public Question()
    {
        Id = 0;
        Questions = string.Empty;
        Answer = string.Empty;
        Level = 0;
        wrongAnswer = new string[2];
    }

    public string print()
    {
        return "Question :: " + Questions + " : " + "answer :: " + Answer + " : " + "wrong answer :: " + wrongAnswer[0] + " " + wrongAnswer[1];
    }
    
}

public class QuestionManager : MonoBehaviour
{

    private static QuestionManager _instance;
    public static QuestionManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<QuestionManager>() as QuestionManager;
            }

            return _instance;
        }
        
    }

    [SerializeField]
    private Question[] QuestionList;
    [SerializeField]
    private GameObject _QuestionCanvas;
    [SerializeField]
    private Text _QuestionText;
    [SerializeField]
    private float TimeSpawned = 1f;

    public TrackManager _trackmanager;
    public UnityEvent QuestionTriggerEvent;
    public UnityEvent QuestionAnswerWrongEvent;
    public UnityEvent QuestionAnswerEvent;
    public Question QuestionPicked { get; private set; }

    private bool _isQUestionAvailable;
    public bool IsQuestionAvailable { get { return _isQUestionAvailable; } }

    private bool _IsQuestionReady;
    private float _QuestionCounter;
    private float _QuestionLimit = 3f;

    private void OnEnable()
    {
        Reset();
        QuestionTriggerEvent.AddListener(OnQuestionTriggered);
        QuestionAnswerEvent.AddListener(OnQuestionAnswer);
    }

    private void OnDisable()
    {
        Reset();
        QuestionTriggerEvent.RemoveListener(OnQuestionTriggered);
        QuestionAnswerEvent.RemoveListener(OnQuestionAnswer);
    }

    private void OnQuestionTriggered()
    {
        var level = 1;
        TriggerQuestion(level);
        _trackmanager.SpawnQuestionAnswer(_trackmanager.segments[1], QuestionPicked);
    }

    private void OnQuestionAnswer()
    {
        _IsQuestionReady = true;
    }

    public void Reset()
    {
        CloseQuestion();
        QuestionPicked = new Question();
        _isQUestionAvailable = false;
        _IsQuestionReady = false;
        _QuestionCounter = 0;

    }

    public void BeginQuestion()
    {
        if (_trackmanager == null)
            throw new NullReferenceException("Please fill the track manager first");
        _IsQuestionReady = true;
    }

    public void EndQuestion()
    {
        Reset();
    }

    private void Update()
    {
        if (_IsQuestionReady == false)
            return;

        if(_QuestionCounter < _QuestionLimit)
        {
            _QuestionCounter += Time.deltaTime;
        }
        else
        {
            _isQUestionAvailable = true;
            _IsQuestionReady = false;
            _QuestionCounter = 0;
        }

        if (_isQUestionAvailable)
        {
            SpawnQuestionAndAnswerTrigger();
        }
    }

    private void SpawnQuestionAndAnswerTrigger()
    {
        //spawn question
        _trackmanager.SpawnQuestionTrigger(_trackmanager.segments[1]);
        _isQUestionAvailable = false;
    }

    public void TriggerQuestion(int level)
    {
        QuestionPicked = PickQuestion(level);
        ShowQuestion(QuestionPicked);
    }

    public Question PickQuestion(int level = 1)
    {
        List<Question> questionDistilled = new List<Question>();

        foreach(Question q in QuestionList)
        {
            if(q.Level == level)
            {
                questionDistilled.Add(q);
            }
        }

        var random = UnityEngine.Random.Range(0, questionDistilled.Count - 1);
        return questionDistilled[random];
    }

    private void ShowQuestion(Question q)
    {
        if (_QuestionCanvas == null)
            return;

        _QuestionText.text = q.Questions;
        _QuestionCanvas.SetActive(true);
        StartCoroutine(ActivateCountDown());
    }

    private IEnumerator ActivateCountDown()
    {

        yield return new WaitForSeconds(TimeSpawned);

        CloseQuestion();
        yield return null;
    }

    private void CloseQuestion()
    {
        if (_QuestionCanvas == null)
            return;

        _QuestionCanvas.SetActive(false);
        _QuestionText.text = string.Empty;
    }
}