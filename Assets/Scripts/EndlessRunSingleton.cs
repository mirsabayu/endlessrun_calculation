﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ScoreClass
{
    public int Id;
    public string Name;
    public int Score;
}

[Serializable]
public class ScoreClassList
{
    public List<ScoreClass> ScoreList;
}

class ScoreComparer : IComparer<ScoreClass>
{
    public int Compare(ScoreClass x, ScoreClass y)
    {
        if(x.Score == 0 || y.Score == 0)
        {
            return 0;
        }

        return x.Score > y.Score ? 1 : 0;
    }
}

public class EndlessRunSingleton : MonoBehaviour
{
    private List<ScoreClass> _ScoreList;
    private static EndlessRunSingleton _instance;

    public static EndlessRunSingleton Instance
    {
        get
        {
            

            return _instance;
        }

    }

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;

        DontDestroyOnLoad(this);

        MusicVolume = 0.1f;
        SFXVolume = 0.1f;
        _ScoreList = new List<ScoreClass>();
    }

    public bool SFXEnabled { get; set; }
    public bool MusicEnabled { get; set; }
    public float SFXVolume { get; set; }
    public float MusicVolume { get; set; }
    public List<ScoreClass> ScoreList { get { return _ScoreList; } }

    public void SaveScoreData(ScoreClass obj)
    {
        if(_ScoreList != null)
            _ScoreList.Add(obj);
    }

    public void SavedAllScoreData()
    {
        SortScoreList();
        DumpScoreList();
    }

    private void SortScoreList()
    {
        var comparer = new ScoreComparer();
        _ScoreList.Sort(comparer);
    }

    private void DumpScoreList()
    {
        var scoreList = new ScoreClassList();
        scoreList.ScoreList = _ScoreList;

        var json = JsonUtility.ToJson(scoreList);
        
        PlayerPrefs.SetString("ScoreSave", json);
    }

    public void QuitGame()
    {
        SavedAllScoreData();
        Application.Quit();
    }

    public void LoadLeaderBoardData()
    {
        _ScoreList = LoadScoreFromFile();
    }

    private List<ScoreClass> LoadScoreFromFile()
    {
        string score = PlayerPrefs.GetString("ScoreSave");
        if (score == null || score == string.Empty)
            return new List<ScoreClass>();

        var scorelist = JsonUtility.FromJson<ScoreClassList>(score);

        return scorelist.ScoreList;
    }


}