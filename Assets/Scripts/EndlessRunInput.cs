﻿using System;
using UnityEngine;
using UnityEngine.Events;

public enum MovementType
{
    NONE,
    LEFT,
    RIGHT
}

public class MovementEvent : UnityEvent<MovementType>
{
    public MovementEvent()
    {

    }
}

public class EndlessRunInput
{
    private static EndlessRunInput _instance;

    public  MovementEvent InputEvt;

    public static EndlessRunInput Instance
    {
        get
        {
           if(_instance == null)
            {
                _instance = new EndlessRunInput();
            }

            return _instance;
        }
    }

    public EndlessRunInput()
    {
        InputEvt = new MovementEvent();
    }

    public MovementType Type { get; private set; }

    public void Tick(float deltaTime)
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            TurnLeft();
        }

        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            TurnRight();
        }
    }


    public void TurnLeft()
    {
        InputEvt.Invoke(MovementType.LEFT);
    }

    public void TurnRight()
    {
        InputEvt.Invoke(MovementType.RIGHT);
    }
}