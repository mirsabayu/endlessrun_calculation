﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InGameOver : AState
{
    [SerializeField]
    private GameObject _Canvas;
    [SerializeField]
    private TrackManager _TrackManager;
    [SerializeField]
    private Text _ScoreText;
    [SerializeField]
    private MusicPlayer _musicPlayer;

    public void Awake()
    {
        _Canvas.SetActive(false);
    }

    public override void Enter(AState from)
    {
        _Canvas.SetActive(true);
        _ScoreText.text = _TrackManager.score.ToString();
        ScoreClass score = new ScoreClass();
        score.Id = new System.Random(4).Next();
        score.Name = "Player";
        score.Score = _TrackManager.score;
        EndlessRunSingleton.Instance.SaveScoreData(score);
    }

    public void QuitGame()
    {
        EndlessRunSingleton.Instance.QuitGame();

    }

    public void RestartGame()
    {
        manager.SwitchState("InGameLoad");
    }

    public void LeaderBoard()
    {

    }

    public override void Exit(AState to)
    {
        _musicPlayer.StopAllStem();
        _TrackManager.End();
        QuestionManager.Instance.EndQuestion();
        _Canvas.SetActive(false);
    }

    public override string GetName()
    {
        return "InGameOver";
    }

    public override void Tick()
    {
    }
}
