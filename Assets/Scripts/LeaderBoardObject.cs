﻿using System;
using UnityEngine;

public class LeaderBoardObject : MonoBehaviour
{
    [SerializeField]
    private GameObject _ScoreListGameObject;
    [SerializeField]
    private ScoreObject _ScoreObject;

    private void Start()
    {
        LoadLeaderBoard();
    }

    public void LoadLeaderBoard()
    {
        EndlessRunSingleton.Instance.LoadLeaderBoardData();
        var scoreList = EndlessRunSingleton.Instance.ScoreList;
        if(scoreList != null)
        foreach (ScoreClass score in scoreList){

            var Scoreobj = GameObject.Instantiate(_ScoreObject) as ScoreObject;
            Scoreobj.SetScore(score.Name, score.Score.ToString());
            Scoreobj.transform.SetParent(_ScoreListGameObject.transform);
            Scoreobj.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
            Scoreobj.transform.localScale = Vector3.one;

        }
    }


}
