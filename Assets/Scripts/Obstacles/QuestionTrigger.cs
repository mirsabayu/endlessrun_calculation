﻿using System;
using UnityEngine;

public class QuestionTrigger : Obstacle
{


    public override void Spawn(TrackSegment segment, float t)
    {
        Vector3 position;
        Quaternion rotation;
        segment.GetPointAt(t, out position, out rotation);
        var obj = Instantiate(gameObject, position, rotation);
        obj.transform.SetParent(segment.objectRoot, true);
    }

    public void OnTriggerEnter(Collider other)
    {
        QuestionManager.Instance.QuestionTriggerEvent.Invoke();
    }
}
