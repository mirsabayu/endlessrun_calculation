﻿using System;
using UnityEngine;
public  class QuestionAnswerWrongTrigger : QuestionAnswerTrigger
{
    public override void Spawn(TrackSegment segment, float t)
    {
        base.Spawn(segment, t);

    }

    protected override void OnTrigger()
    {
        QuestionManager.Instance.QuestionAnswerWrongEvent.Invoke();
    }
}
