﻿using System;
using UnityEngine;


public class QuestionAnswerTrigger : Obstacle
{
    protected const int k_LeftMostLaneIndex = -1;
    protected const int k_RightMostLaneIndex = 1;

    [SerializeField]
    protected TextMesh _Text;

    protected string _question;
    protected int _lane;

    public void Spawn(TrackSegment segment, float t, int lane, string text)
    {
        _question = text;
        _lane = lane;
        Spawn(segment, t);
        
    }

    public override void Spawn(TrackSegment segment, float t)
    {
        Vector3 position;
        Quaternion rotation;
        segment.GetPointAt(t, out position, out rotation);

        _Text.text = _question;
        GameObject obj = Instantiate(gameObject, position, rotation);
        obj.transform.SetParent(segment.objectRoot, true);
        obj.transform.position += obj.transform.right * _lane * segment.manager.laneOffset;
        Debug.Log("answer position" + obj.transform.position + " : "  + _lane);
        

    }

    private void OnTriggerEnter(Collider other)
    {
        OnTrigger();
    }

    protected virtual void OnTrigger()
    {
        QuestionManager.Instance.QuestionAnswerEvent.Invoke();

    }
}