﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mainMenuSetting : MonoBehaviour
{
    [SerializeField]
    private AudioSource _audio;

    public void AturMusic(float volume)
    {
        EndlessRunSingleton.Instance.MusicVolume = volume;
        _audio.volume = volume;
    }

    public void AturSound(float volume)
    {
        EndlessRunSingleton.Instance.SFXVolume = volume;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
