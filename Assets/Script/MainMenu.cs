﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{
    public GameObject subMenu;

    public GameObject smCredits;
    public GameObject smOptions;
    public GameObject smLeaderBoard;

    public Text tTitle;

    public AudioSource music;
    public Text tMusic;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void SubMenu()
    {
        if (subMenu.activeInHierarchy == false)
        {
            tTitle.text = EventSystem.current.currentSelectedGameObject.name;
            if (tTitle.text == "Credits")
            {
                smCredits.SetActive(true);
            }
            else if(tTitle.text == "Settings")
            {
                smOptions.SetActive(true);
            }else
            {
                smLeaderBoard.SetActive(true);
            }

            subMenu.SetActive(true);
        }
        else
        {
            if (tTitle.text == "Credits")
            {
                smCredits.SetActive(false);
            }   
            else if(tTitle.text == "Settings")
            {
                smOptions.SetActive(false);
            }
            else 
            {
                smLeaderBoard.SetActive(false);
            }

            subMenu.SetActive(false);
        }
    }

    public void Music()
    {
        if (music.mute == false)
        {
            tMusic.text = "Music: Off";

            music.mute = true;
        }
        else
        {
            tMusic.text = "Music: On";

            music.mute = false;
        }
    }

    public void LeaderBoard ()
    {
       
    }

    public void ExitGame()
    {
        EndlessRunSingleton.Instance.QuitGame();
    }
}
